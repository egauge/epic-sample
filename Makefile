SRCS = $(shell find epic-sample/ -name \*.py)

all:
	python3 setup.py sdist

release: all
	twine upload dist/*

lint:
	pylint $(SRCS)
